# Manejo de estilos . CSS en Next.js

## CSS Global

La forma más sencilla para empezar a incorporar `CSS` en Next.js, es comenzar por su `CSS` global.

El archivo `globals.css` que se encuentra en la ruta: `./src/app`, tiene todos los estilos que se aplicarán en forma `global` a nuestra aplicación.

Por defecto ya viene con una configuración de estilos que podemos empezar a cambiar y ver los resultados en la ejecución de nuestra aplicación.

La llamada a este archivo está en el `layout.js` dentro de la carpeta `app` (`./src/app/layout.js o jsx`)

***``./src/app/globals.css``***
```css
html,
body{
  background-color: antiquewhite;
  padding: 0;
  margin: 0;
  font-family: Verdana, Geneva, Tahoma, sans-serif;
}

*{
  box-sizing: border-box;
  color: inherit;
  text-decoration: none;
}
```

## CSS Modular o CSS Modules

Es una forma de encapsular el `CSS` de tal manera que sea único para un determinado componente o página.

Todo archivo de `css module` deberá llevar la extensión `.module.css`, esto hará que al importar el archivo en el componente en donde se desea aplicar, nos devolverá un `objeto` y dentro tantas propiedades como estilos se hayan aplicado.

Veamos esto con un ejemplo:

👉**Paso 1**
Vamos a crear un nuevo archivo llamado `page.module.css` dentro de `./src/app`, y copiemos el siguiente fragmento de código:

**`./src/app/page.module.css`**
```css
.container{
    display: flex;
    justify-content: center;
    align-content: space-around;
}

.parrafo{
    margin: 15px auto;
    font-family: Arial, Helvetica, sans-serif;
}
```

👉**Paso 2**
Modifiquemos nuestro archivo `page.jsx` que contiene la función `Home()`, agregando el siguiente código:

**`./src/app/page.jsx`**
```javascript
//...
<main>
      <h1>Hola Mundo</h1>
      <div className={styles.container}>
        <div className={styles.parrafo}>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium
          at, architecto exercitationem nobis praesentium quis accusantium
          dolorem harum, commodi repudiandae, temporibus earum impedit delectus
          iste aliquid dolorum magnam quibusdam in.
        </div>
        <div className={styles.parrafo}>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laudantium
          quod sequi commodi veritatis cumque ab error facilis ipsa quae
          mollitia. Minus ab omnis aut, debitis eum facere doloremque quia
          pariatur.
        </div>
      </div>
</main>
//...
```

👉**Paso 3**
Importemos ahora nuestro archivo `page.module.css`  en una variable llamada `styles`.

**`./src/app/page.jsx`**
```javascript
import styles from './page.module.css';

//...

```

Deberá quedar como se muestra a continuación:

**`./src/app/page.jsx`**
```javascript
import styles from './page.module.css';

export default function Home() {
  return (
    <main>
      <h1>Hola Mundo</h1>
      <div className={styles.container}>
        <div className={styles.parrafo}>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium
          at, architecto exercitationem nobis praesentium quis accusantium
          dolorem harum, commodi repudiandae, temporibus earum impedit delectus
          iste aliquid dolorum magnam quibusdam in.
        </div>
        <div className={styles.parrafo}>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laudantium
          quod sequi commodi veritatis cumque ab error facilis ipsa quae
          mollitia. Minus ab omnis aut, debitis eum facere doloremque quia
          pariatur.
        </div>
      </div>
    </main>
  );
}
```

Guardemos los cambios y veamos el resultado en el navegador.

# Optimización de fuentes con Next.js

La optimización de fuentes es una técnica que consiste en cargar y mostrar las fuentes de texto de manera eficiente y la privacidad de un sitio web. Con Next.js, esta optimización se vuelve aún más accesible utilizando next/font.

## Autohospedaje de Fuentes

Es la capacidad de cargar tus fuentes de manera óptima y eficiente directamente desde tu propio servidor web, eliminando la necesidad de solicitudes externas, lo que mejora la privacidad y el rendimiento de tu sitio.

Supongamos que necesitamos utilizar la fuente `“Inter”` en nuestra app. Puede hacerse de la siguiente manera:

```javascript
import { Inter } from 'next/font/google'
 

const inter = Inter({ subsets: ['latin'] })
 
export default function MyApp() {
  return (
    <main className={inter.className}>
      /*componentes*/
    </main>
  )
}
```


# Optimización de imágenes

Next.js optimiza la carga de imágenes de forma inteligente. Las imágenes dentro de la ventana gráfica se cargan primero, lo que acelera la carga inicial de la página. Las imágenes que están por debajo se cargan de forma asíncrona a medida que el usuario se desplaza hacia abajo. 

Cuenta también con un componente `<Image />` que optimiza las imágenes y evita problemas de tipo CLS (_Cumulative Layout Shift_) que es un factor de `Core Web Vitals` importante para el posicionamiento.


## Componente Image

El componente `<Image />` de Next.js también ofrece las siguientes funciones de optimización de imágenes:

- Tamaño: Sirve automáticamente imágenes del tamaño correcto para cada dispositivo, utilizando formatos de imagen modernos como WebP y AVIF.

- Estabilidad visual: Evita el cambio de diseño.

- Cargas de página más rápidas: las imágenes solo se cargan cuando ingresan a la ventana gráfica mediante la carga diferida del navegador nativo, con marcadores de posición borrosos opcionales.

- Flexibilidad de recursos: cambio de tamaño de imágenes bajo demanda, incluso para imágenes almacenadas en servidores remotos

### Uso
Para poder hacer uso del componente `<Image />`, debemos seguir estos pasos:


👉**Paso 1**
En el componente o página en donde queremos mostrar/cargar/insertar la imagen, debemos importar el componente `<Image/ >`:

```javascript
import Image from 'next/image';
```

👉**Paso 2**
Por otro lado necesitamos el archivo de imagen que queremos insertar, que al ser un archivo estático lo vamos a tener en la carpeta `./public`.

👉**Paso 3**
Importamos el archivo (jpg, png, svg... etc.) en el componente en donde se va insertar:

```javascript
import Image from 'next/image';
import imagen_a_insertar from '../public/imagen_a_insertar.jpg';
```

👉**Paso 4**
Y por último hacer uso del componente `<Image />`, esto es, insertarlo en el lugar dentro de la página o componente en donde se lo quiere mostrar:

```javascript
import Image from 'next/image';
import imagen_a_insertar from '../public/imagen_a_insertar.jpg';

export default function NombreComponente() {
  return (
    <Image
      src={imagen_a_insertar}
      alt="Picture of the author"
      width={500} 
      height={500}
      //otras propiedades
    />
  )
}
```
