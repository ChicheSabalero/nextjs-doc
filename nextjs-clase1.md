# ¿Qué es Next.js?

Next.js es un framework de React, creado por Vercel, que se ha vuelto muy popular en los últimos años.

Permite a los desarrolladores crear aplicaciones SPA (Single Page Applications) y aplicaciones web de alto rendimiento a través de la renderización del lado del servidor (SSR).

## Por qué utilizarlo y cuáles son sus beneficios.

Algunos de los beneficios más destacados de usar este framework son:

### 👉Renderizado del lado del servidor (SSR)

Es una técnica que permite que el contenido se envíe al navegador ya renderizado, lo que mejora la velocidad de carga de la página y la experiencia del usuario. Esto significa que el navegador no tiene que esperar a que se cargue todo el JavaScript antes de mostrar la página.

### 👉Pre-renderizado estático

Next.js permite pre-renderizar las páginas en HTML estático, esto hace que las páginas se carguen rápidamente y sean más fáciles de indexar para los motores de búsqueda. Es útil para sitios web que no requieren interacciones dinámicas en el lado del cliente.

El pre-renderizado estático mejora el SEO al permitir que los motores de búsqueda indexen el contenido más fácilmente.

### 👉Enrutamiento automático

El enrutamiento automático está basado en la estructura de archivos del proyecto. Esto significa que se puede agregar una nueva página simplemente creando un archivo en una carpeta específica, sin tener que preocuparse por configurar rutas de manera explícita.

### 👉Compatibilidad con React

Al estar construido sobre React, Next es compatible con todas sus características y se puede usar para construir aplicaciones complejas y escalables. Esto significa que los desarrolladores pueden aprovechar todas las ventajas de React mientras usan Next para simplificar el proceso de desarrollo.

### 👉Soporte para CSS-in-Js

CSS-in-JS es un patrón que permite escribir CSS usando JavaScript en lugar de definirlo en archivos externos.

Esto en React es conocido como `Styled Components` y básicamente se trata de escribir el CSS directamente en el componente como una función más de JavaScript.

Next tiene soporte integrado para varias bibliotecas de CSS-in-JS, lo que facilita la gestión del estilo del proyecto. Se pueden crear y gestionar los estilos en el mismo archivo que el código JavaScript, lo que simplifica el proceso de desarrollo y hace que el proyecto sea más fácil de mantener.

### 👉Integración APIs

Next.js se integra fácilmente con APIs, lo que permite construir aplicaciones web completas y dinámicas. Esto significa que los desarrolladores pueden agregar funciones de backend a su aplicación sin tener que preocuparse por la complejidad de la integración.

## CSR vs SSR

Para poder entregar la mejor experiencia al usuario, uno de los puntos es el tiempo de renderizado, donde si una página se demora de entre 1 a 3 segundos en cargar, la posibilidad de que el usuario salga de la página es de 32% y un 106% si aumentamos a 6 segundos. Esto según los <a href="https://support.google.com/webmasters/answer/9205520?hl=es">Core Web Vitals de Google</a>.<br>
Para evitar esto, necesitamos que las páginas carguen rápido y sean interactivas rápidamente. Para que esto suceda, podemos abordar estos casos de diferentes formas.

### Client Side Rendering (SCR)

Es posiblemente, el más común entre los iniciados en el mundo de React.js y Vue.js, es el estado por defecto de las aplicaciones web realizadas con librerías y frameworks Front-end actuales.

Su funcionamiento es muy simple:<br>
_1_ El cliente (navegador) hace la petición al servidor web donde se encuentra alojada<br>
_2_ El servidor entrega una página en "blanco"<br>
_3_ El navegador comienza a cargar el contenido `JavaScript` y comienza hacer a las APIs para obtener la información<br>
_4_ Cuando se obtiene la información la página puede empezar a "pintar" la data volviéndose interactiva para el usuario.

<img src="./images/csr.jpg">

### Server Side Rendering (SSR)

Este modelo como su nombre lo indica, crea la página final en el lado del servidor, llenándola con información previo a entregársela al usuario.<br>
Su funcionamiento es el siguiente:<br>
_1_ El cliente (navegador) hace la petición al servidor web donde se encuentra alojada<br>
_2_ React corre en el servidor ejecutando todas las llamadas a las APIs para obtener la información que requiere la página<br>
_3_ Las APIs entregan la información requerida por el servidor<br>
_4_ Con el HTML montado, el servidor entrega la página al cliente<br>
_5_ Cuando llega la página al cliente, React compara la pagina entregada con el `javascript` precargado y genera el virtual DOM, si es exitoso ambas partes se reconcilian y está lista para ser usada por el usuario <br>

<img src="./images/ssr.jpg">

# Arquitectura de un proyecto con Next.Js

Comprendido la diferencia entre SCR y SSR, nos enfocaremos ahora como construir aplicaciones con `Next.js`.

## Instalación y estructura de carpetas

### Instalación: Pasos para crear un nuevo proyecto

Crear un proyecto `Next.js` es similar a crear un proyecto con `Vite` visto en el módulo de `React` durante la cursada de dicho módulo.

👉**Paso 1**
En el directorio/carpeta donde vamos a trabajar abrimos una terminal de `git`, `powershell` o la terminal que use de acuerdo a su sistema operativo, si usa `Windows` debe usar el `powershell` y ejecutaremos la siguiente instrucción:

```
npx create-next-app@latest
```

Durante la instalación veremos las siguientes indicaciones:

```
What is your project named? my-app (nombre de la aplicación)
Would you like to use TypeScript? No / Yes (si queremos TypeScript por el momento (No))
Would you like to use ESLint? No / Yes (si queremos a usar ESLint (si))
Would you like to use Tailwind CSS? No / Yes (Si queremos usar Tailwind en este caso (No))
Would you like to use `src/` directory? No / Yes (creación de la carpeta src (si))
Would you like to use App Router? (recommended) No / Yes (Opción recomendada (si))
Would you like to customize the default import alias (@/*)? No / Yes (Customizar alias (si))
What import alias would you like configured? @/*
```

Hecho esto, `create-next-app` creará una carpeta con el nombre de su proyecto e instalará las dependencias requeridas.

👉**Paso 2**
Nos posicionaremos en la carpeta que `next` acaba de crear y abriremos el proyecto en `VS Code` o el editor que use y/o prefiera.

```
cd primera-app-next
code .
```

👉**Paso 3**
En `VS Code` abramos un nuevo terminal, `Ctrl+Mayus+ñ` o menú `Terminal` --> `Nuevo terminal`, y ejecutemos la instrucción `npm run dev`

```
npm run dev
```

Esto levantará el proyecto en nuestro `localhost`.

```
$ npm run dev

> primera-app-next@0.1.0 dev
> next dev

   ▲ Next.js 14.1.3
   - Local:        http://localhost:3000

 ✓ Ready in 12.2s
```

Abramos un navegador y pongamos la `url` `http://localhost:3000
`
Si todo esta correcto nos debería mostrar la página inicaial de `Next.js`, algo muy similar a lo que ocurría con `Vite` y `React`.

Puede que la primera vez que se cargue, demore un poco mas de lo "normal" ya que se tienen que compilar el proyecto.

Además esto creará una nueva carpeta llamada `.next` que describiremos más adelante.

Antes de continuar con el estudio de la estructura de carpetas que `create-next-app` vamos aclarar que es esto de `App Router` que recomienda `Next`.
¿Que otra opción nos da Next?, pues bien, algo llamado `Pages Router`

### App Router vs Pages Router

La elección entre el `App Router` y el `Pages Router` en `Next.js` depende de las necesidades y la complejidad del proyecto. El Pages Router es más simple y permite un desarrollo rápido, mientras que el App Router ofrece un mayor control y flexibilidad para necesidades de enrutamiento más complejas.

### Estructura del proyecto (descripción)

#### Carpeta .next

Es una carpeta que por defecto está `invisible`. Inicialmente esta carpeta puede que no esté presente en el proyecto, o si el proyecto fue clonado desde algún repositorio, ya que esta carpeta se genara automáticamente cuando hacemos el `build de producción (npm run build)` o el `build de desarrollo (npm run dev)`. Muy rara vez se modifican o se tocan los archivos generados dentro de ella ya que se trata de una carpeta para despliegue de la aplicación.

#### Carpeta app (`./src/app`)

Esta carpeta contiene todo lo relacionado a como queremos que `next` maneje nuestro sistema de rutas de la aplicación.

#### Archivo page.js (`./src/app/page.js` o `.jsx`)

Es el archivo que sirve nuestra aplicación cuando alguien entra por la ruta `slash` o `home`.
Es decir es el archivo por el cual se inicia nuestra aplicación.

#### Carpeta Public(`./public`)

Al igual que en una aplicación `React` esta carpeta es utilizada para servir archivos estáticos, ejemplo: imágenes, archivos `css`, `html` o de `JavaScript`, es decir archivos que no van a ser compilados, simplemente van a ser servidos desde esta carpeta.

## Creación de rutas en Next.js

### Router

En Next.js no se necesita interactuar directamente con el _router_ para crear y ver páginas en la app o sitio que se esté desarrollando. Next.js se ha construido con convenciones para hacer el proceso de creación de rutas tan fácil como crear un archivo.

👉**Paso 1**
Para comenzar, vamos a crear una carpeta `about` dentro de la carpeta **`./src/app/about`** y dentro de ella un archivo `.js`, o `.jsx` que simplemente retornará una `UI` con un mensaje:

**Debería verse algo así**

<img src="./images/cap-arbol-1.jpg">

👉**Paso 2**
En el archivo `page.jsx` copiaremos el siguiente fragmento de código:

**`./src/app/about/page.jsx`**

```javascript
export default function About() {
  return (
    <>
      <h2>Soy About page</h2>
    </>
  );
}
```

Al igual que `React` una página o componente no es más que una función de `javascipt` que hace un `return` de una `UI`

👉**Paso 3**
En la `url` del navegador simplemente escribimos `http://localhost:3000/about` y listo ya tenemos nuestra primera ruta.

**¿Te animas a crear una ruta para contactos?**

### Subruta y ruta dinámica

Para crear sub-rutas seguiremos la misma lógica anterior. Crear carpeta con un archivo `page` dentro de cada carpeta.

Ejemplo: vamos a crear una ruta para mostrar un listado de clientes o podría ser de usuarios, o de películas, como siempre esto a modo de ejemplo.

👉**Paso 1**
Crearemos una carpeta llamada `clients` dentro de **`./src/app`** y dentro de esta carpeta su archivo `page.jsx`

Debería quedarnos algo así:

<img src="./images/cap-arbol-2.jpg">

👉**Paso 2**
Dentro del archivo `page.jsx`, copiaremos el siguiente código:

**`./src/app/clients/page.jsx`**

```javascript
export default function Clients() {
  return (
    <>
      <h2>Listado de clientes</h2>
    </>
  );
}
```

Supongamos ahora que necesitamos otra ruta que dependa de `clients`, pero en este caso para mostrar un cliente determinado, algo así como un `detail` del cliente. Que cuando accedamos a la misma le pasemos como parámetro el `id` del cliente para que haga lo suyo `acceder a una API con el id y traernos la info` (por el momento solo prepararemos la ruta.... no nos adelantemos). Esto recibe el nombre de `ruta dinámica`

👉**Paso 1**
Dentro de **`./src/app/clients`** vamos a crear una carpeta entre corchetes (`[]`) con el nombre del parámetro al que vamos a procesar. En este caso `[id]`. Y dentro de ella el archivo `page.jsx`.
Debería verse así:

<img src="./images/cap-arbol-3.jpg">

👉**Paso 2**
Dentro del archivo `page` copiaremos el siguiente código:

**`./src/app/clients/[id]/page.jsx`**

```javascript
export default function ClientDetail() {
  return (
    <>
      <h2>Detalle del cliente:</h2>
    </>
  );
}
```

👉**Paso 3**
Probemos en el navegador que esto funcione: `http://localhost:3000/clients/1`

Si podemos ver el mensaje: `Detalle del cliente:`, entonces vamos por el camino correcto.

Ahora, ¿Como hacemos para tomar ese parámetro en nuestra página de detalle?

Así como en `React` existen las `props`, en Next.js ¿también?... claro! también existen ya que es un framework basado en React!.

Modifiquemos un poco el código anterior.

**`./src/app/clients/[id]/page.jsx`**

```javascript
export default function ClientDetail(props) {
  console.log(props);
  return (
    <>
      <h2>Detalle del cliente:</h2>
    </>
  );
}
```

Guardemos el archivo con el cambio y observemos la salida de la consola.

Nos debería mostrar algo así:

```
{ params: { id: '1' }, searchParams: {} }
```

Como se puede ver `props` es un objeto que dentro de el vienen los `params` y los `searchParams`, por el momento solo nos concentraremos en el objeto `params`.

**`./src/app/clients/[id]/page.jsx`**

```javascript
export default function ClientDetail({ params: { id } }) {
  return (
    <>
      <h2>Detalle del cliente con id: {id} </h2>
    </>
  );
}
```

```
Para ampliar información sobre rutas dinámicas puede acceder a la documentación oficial de Next.js con este enlace:
https://nextjs.org/docs/app/building-your-application/routing/dynamic-routes
```

## Layouts

Un _layout_ en `Next.js` es un componente de `IU` que se comparte entre varias páginas de una aplicación. Permite definir una estructura y apariencia común para un grupo de páginas, reduciendo la redundancia y promoviendo la reutilización de código.

Siguen siempre la misma estructura, el nombre del archivo siempre debe ser `layout.jsx` y debe crearse en la carpeta que contiene la/las rutas a compartir.

Recibe como parámetro un `children` que debe ser devuelto junto con la UI que se quiere representar.

```javascript
export default function NombreLayout({ children }) {
  return (
    <>
      //estructura html a representar //ej: un menú de opciones específico para
      esa ruta/page
      {children}
    </>
  );
}
```

## Navegación entre rutas: componente `Link`

El componente Link es similar al tag `<a>` en HTML y se encuentra en el módulo next/link. Este componente nos permite realizar enrutamiento del lado del cliente (_client-side_).

Ejemplo:

```javascript
import Link from 'next/link';

//....

<Link href="/contacts">Contacts</Link>;
```
